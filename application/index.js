const TelegramBot = require('node-telegram-bot-api');
const token = process.env.TELEGRAM_TOKEN; //The telegram bot token, provided by bot father
const bot = new TelegramBot(token, {polling: true});
var mongoose = require('mongoose');
var UserService = require('./user-service');

mongoose.connect(process.env.MONGODB_URI);

//Error Handling
var nonExistentUserId = 12;
bot.sendMessage(nonExistentUserId, 'text').catch((error) => {
  console.log(error.code);  // => 'ETELEGRAM'
  console.log(error.response.body); // => { ok: false, error_code: 400, description: 'Bad Request: chat not found' }
});


bot.onText(/\/start/, (msg, match) => { // Listener:: When user sends the bot command "/start"
  bot.sendMessage(msg.from.id, "Sample text message", {
     "reply_markup":{
      "keyboard":[["/Button_1"],[["/Button_2"],["Button_3"]],["Button_4"]]  //Gives you buttons in keypad instead of a normal text keypad
    }
  });
});

bot.onText(/\/say_hello/, (msg, match) => { // Listener:: When user sends the bot command "/say_hello"
  bot.sendMessage(msg.from.id, "Hello!");
});

bot.onText(/\/style_text/, (msg, match) => { // Listener:: When user sends the bot command "/style_text"
  var content = '<b>bold</b>, <strong>bold</strong>'+
                '<i>italic</i>, <em>italic</em>'+
                '<a href="http://www.example.com/">inline URL</a>'+
                '<code>inline fixed-width code</code>'+
                '<pre>pre-formatted fixed-width code block</pre>';
  bot.sendMessage(msg.from.id, content, {
    parse_mode : "HTML"
  });
});

bot.onText(/\/addMe (.+)/, (msg, match) => {
  /* Connecting to DB */
    var userdata = {};
    userdata.firstName = msg.from.first_name;
    userdata.lastName = msg.from.last_name;
    userdata.userTgID = msg.from.id;
    userdata.userTgHandle = msg.from.username;

    UserService.schema.methods.addUser(userdata, addSuccess, addFail);
});

bot.onText(/\/removeMe (.+)/, (msg, match) => {
  /* Connecting to DB */
    var userTgID = msg.from.id;
    UserService.schema.methods.removeUser(userTgID, removeSuccess, removeFail);
});

/* For any message sent to the bot */
bot.on('message', (msg) => {
  /*The 'msg' here has lot of useful information, in for of a JSON.
    * Like telegram id of the sender
    * telegram id of the receiver/group
    * The message
    * Type of message - bot command/url/text etc..
  */
});


/* Custom Success/Fail function */
function addSuccess(/*Parameters if any */){
  /*Send success message to the user or what ever you want */
}

function addFail(/*Parameters if any */){
  /*Send failure message to the user or what ever you want */
}

function removeSuccess(/*Parameters if any */){
  /*Send success message to the user or what ever you want */
}

function removeFail(/*Parameters if any */){
  /*Send failure message to the user or what ever you want */
}