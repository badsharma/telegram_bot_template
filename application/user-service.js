'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName:String,
    lastName: String,
    userTgID: Number,
    userTgHandle:String
});

/**
* Methods
*/
UserSchema.methods = {
  "addUser": function(userData, successCallback, failCallBack){
    UserMongo.findOne({userTgHandle:userData.userTgHandle}, function(err, data){
      if(!err && data != null){
        /* User already present */
      }
      else{
        /* New User */
        data.push(userData);
        data.save(function(err){
          if(!err){
            successCallback(/*Parameters if any */)
          }
          else{
            // console.log("ERROR IN User Addition!");
            failCallBack(/*Parameters if any */);
          }
        });
      }
    });
  },
  "removeUser":function(userTelegramId, successCallback, failCallBack){
    UserMongo.find({userTgID:userTelegramId}).remove().exec(function(err, data){
      if(!err){
        /*User Found - Record deleted*/
        successCallback(/*Parameters if any */);
      }
      else{
        /*User Not Found*/
        failCallBack(/*Parameters if any */);
      }
    });
  }
};

/**
 *Indexes
 */
//None Needed

var UserMongo = mongoose.model('user', UserSchema);
module.exports = UserMongo;